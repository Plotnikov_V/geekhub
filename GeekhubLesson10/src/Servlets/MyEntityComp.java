package Servlets;

import java.util.Comparator;

/**
 * Created by root on 31.01.2015.
 */
public class MyEntityComp implements Comparator<My_Entity> {
    @Override
    public int compare(My_Entity ob1, My_Entity ob2) {
        return ob1.getType().compareTo(ob2.getType());
    }
}
