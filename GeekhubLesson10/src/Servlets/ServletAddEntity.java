package Servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 31.01.2015.
 */
@WebServlet(name = "ServletAddEntity", urlPatterns = {"/save"})
public class ServletAddEntity extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.print("<h1> Servlet save . . . </h1>");

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/geekdb10", "root", "root");
            PreparedStatement ps = connection.prepareStatement("INSERT INTO entity(type, name) VALUES (?, ?)");
            ps.setString(1, request.getParameter("new_type"));
            ps.setString(2, request.getParameter("new_name"));
            ps.executeUpdate();

            response.sendRedirect("view");

            connection.close();
        } catch (SQLException e) {out.print("<h3>" + e +"</h3>");}
          catch (ClassNotFoundException e) {e.printStackTrace();}

        out.close();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
