package Servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.*;


/**
 * Created by root on 29.01.2015.
 */
@WebServlet(name = "ServletConnectDB",  urlPatterns = {"/view"})
public class ServletConnectDB extends HttpServlet {
    public void init(){
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.print("<h1> Servlet view . . . </h1>");
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/geekdb10", "root", "root");
            Statement statement = connection.createStatement();
            ResultSet results = statement.executeQuery("SELECT * FROM entity");

            List<My_Entity> entitys = new ArrayList<My_Entity>();
            while (results.next()) {
                My_Entity entity = new My_Entity();
                entity.setType(results.getString("type"));
                entity.setName(results.getString("name"));
                entity.setId_entity(results.getInt("id"));
                entitys.add(entity);
            }
            Collections.sort(entitys, new MyEntityComp());

            request.setAttribute("data_list", entitys);
            RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
            if (dispatcher != null) {
                dispatcher.forward(request,response);
            }

            connection.close();
        } catch (SQLException e) {out.print("<h3>" + e +"</h3>");}
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        out.close();
    }
}


