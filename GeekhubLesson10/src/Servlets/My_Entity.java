package Servlets;

/**
 * Created by root on 30.01.2015.
 */
public class My_Entity {
    private String type;
    private String name;
    private int id_entity;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId_entity() {
        return id_entity;
    }

    public void setId_entity(int id_entity) {
        this.id_entity = id_entity;
    }


}
