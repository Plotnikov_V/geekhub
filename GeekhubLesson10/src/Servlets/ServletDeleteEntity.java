package Servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by root on 31.01.2015.
 */
@WebServlet(name = "ServletDeleteEntity", urlPatterns = {"/delete"})
public class ServletDeleteEntity extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/geekdb10", "root", "root");
            PreparedStatement ps = connection.prepareStatement("DELETE FROM entity WHERE id=?");
            ps.setString(1, request.getParameter("id_delete"));
            ps.executeUpdate();

            response.sendRedirect("view");

            connection.close();
        } catch (SQLException e) {e.printStackTrace();}
        catch (ClassNotFoundException e) {e.printStackTrace();}

    }
}
