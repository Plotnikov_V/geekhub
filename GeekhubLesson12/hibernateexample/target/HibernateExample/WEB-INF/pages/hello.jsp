<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<body>
	<h1>Hibernate Example</h1>
	<form action="${pageContext.request.contextPath}/users" method="post">
		<input name="firstName" placeholder="First Name">
		<input name="lastName" placeholder="Last Name">
		<input name="email" type="email" placeholder="Email">
		<select name="kind_group">
			<c:forEach var="group" items="${groups}">
				<option value="${group.id}">${group.name}</option>
			</c:forEach>
		</select>

		<input type="submit" value="Submit">
	</form>

	<p></p>
	<form action="${pageContext.request.contextPath}/groups" method="post">
		<input name="groupName" placeholder="Group Name">

		<input type="submit" value="Add">
	</form>



	<table border="1">
		<tr>
			<td>ID</td>
			<td>FIRST NAME</td>
			<td>LAST NAME</td>
			<td>EMAIL</td>
			<td>GROUP</td>
		</tr>

		<c:forEach var="user" items="${users}">
		<tr>
			<td>${user.id}</td>
			<td>${user.firstName}</td>
			<td>${user.lastName}</td>
			<td>${user.email}</td>
			<td>${user.group.name}</td>
			<td>
				<form action="${pageContext.request.contextPath}/userEdit" method="post">
					<input type="submit" value="Edit">
					<input type="hidden" name="idUserEdit" value=${user.id} >
				</form>
			</td>
			<td>
				<form action="${pageContext.request.contextPath}/userDel" method="post">
					<input type="submit" value="Delete">
					<input type="hidden" name="index" value=${user.id} >
				</form>
			</td>
		</tr>
		</c:forEach>

	</table>
</body>
</html>