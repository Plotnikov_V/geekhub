<%--
  Created by IntelliJ IDEA.
  User: root
  Date: 16.02.2015
  Time: 19:10
  To change this template use File | Settings | File Templates.
  <%@ page contentType="text/html;charset=UTF-8" language="java" %>

--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Edit User</title>
</head>
<body>
  <h1>Hibernate Example</h1>
  <h3>Edit User</h3>
  <form action="${pageContext.request.contextPath}/Edit" method="post">
    <input name="firstName" value="${UserForEdit.firstName}">
    <input name="lastName" value="${UserForEdit.lastName}">
    <input name="email" type="email" value="${UserForEdit.email}">
    <input type="hidden" name="idUserEdit" value=${UserForEdit.id} >

    <input type="submit" value="Save">
  </form>

</body>
</html>
