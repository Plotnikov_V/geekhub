import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by root on 23.01.2015.
 */
@WebServlet(name = "Servlet", urlPatterns = {"/opendir"})
public class Servlet extends HttpServlet {
  /*  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    } */

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.print("<h1>Hello Servlet Vov@n . . . </h1>");
        out.print("<h1>"+request.getParameter("dir")+"</h1>");

        String dirname = request.getParameter("dir") + "/";

        File f1 = new File (dirname);
        if (f1.getParent() != null) {
            //out.print("<h3>"+"Parent Dir  "+f1.getParent()+"</h3>");
            out.print("<a href=\"opendir?dir="+ f1.getParent() + "\">" + "...up... " + "</a>");
        }
        if (f1.isDirectory()) {
            //out.println(dirname);
            String s[] = f1.list();
            out.print("<UL>");
            for (int i=0; i < s.length; i++){
                File f = new File (dirname + "/" + s[i]);
                if (f.isDirectory()) {
                    out.println("<LI>"  + "<font color=\"blue\">" + "Dir________" + "<a href=\"opendir?dir="+dirname+s[i]+"\">"+ s[i] +"</a>" +"</LI>");
                } else {
                    if ((s[i]).endsWith(".txt")) {
                        out.print("<LI>" + "<font color=\"green\">" + "File_______" + "<a href=\"openfile?filepath=" + dirname + "&filename=" + s[i] + "\">" + s[i] + "</a>" + "<a href=\"deletefile?filepath=" + dirname + "&filename=" + s[i] + "\">  (Press for Delete File) </a>" +"</LI>");
                        //out.println("<p> \"<font color=\"green\">\" <input type=\"submit\" name=\"delete\" value=\"sime\" ></p>");
                        //out.println("<a href=\"deletefile?filepath=" + dirname + "&filename=" + s[i] + "\"> Delete File </a>");
                    } else {
                        out.println("<LI>" + "<font color=\"black\">" + "File_______" + s[i] + "</LI>");
                    }
                }
                }
            out.print("</UL>");
        } else
            out.println("<h1> NOT DIR </h1>");
       /*<form action="createfile" method="post">
            Enter file name for creating <input type="text" name="filename">
            <input type="submit" name="create" value="Create">
            <input type="hidden" name="current_dir" value="">
         </form>   */
        out.print("<font color=\"black\">");
        out.print("<form action=\"createfile\" method=\"post\">");
        out.print("Enter file name for creating <input type=\"text\" name=\"filename\">");
        out.print("<input type=\"submit\" name=\"create\" value=\"Create\">");
        out.print("<input type=\"hidden\" name=\"current_dir\" value=\"" + dirname + "\">");
        out.print("</form>");


    }
}
