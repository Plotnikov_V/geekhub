import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by root on 25.01.2015.
 */
@WebServlet(name = "ServletOpenFile", urlPatterns = {"/openfile"})
public class ServletOpenFile extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<h2>Open File </h2> ");
        //out.println(request.getParameter("filepath"));
        //out.println(request.getParameter("filename"));
        BufferedReader breader = new BufferedReader(new FileReader(request.getParameter("filepath")+request.getParameter("filename")));
        StringBuilder sbu = new StringBuilder();
        String line;
        while ((line = breader.readLine()) != null) {
            sbu.append(line);
        }
        breader.close();

        out.println("<textarea id=\"text\" cols=\"150\" rows=\"20\">" + sbu + "</textarea>");

        out.println("<p> </p>");
        out.print("<a href=\"opendir?dir=" + request.getParameter("filepath") + "\"> Press for back </a>");
        out.flush();
        out.close();

    }
}
