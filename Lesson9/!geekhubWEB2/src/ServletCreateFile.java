import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by root on 24.01.2015.
 */
@WebServlet(name = "ServletCreateFile", urlPatterns = {"/createfile"})
public class ServletCreateFile extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        if (request.getParameter("filename").equalsIgnoreCase("")) {
            out.println("<p>you do not enter a file name </p> ");
            out.print("<a href=\"opendir?dir=" + request.getParameter("current_dir") + "\"> Press for back </a>");
            //out.print("<a href=\"index.jsp\"> Back </a>");
        } else {
            File fl = new File(request.getParameter("current_dir")+request.getParameter("filename"));
            try {
                fl.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            out.print("<p>DONE!</p>");
            out.print("<a href=\"opendir?dir=" + request.getParameter("current_dir") + "\"> Press for back </a>");
        }
    out.flush();
    out.close();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
