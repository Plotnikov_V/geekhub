import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by root on 25.01.2015.
 */
@WebServlet(name = "ServletDeleteFile", urlPatterns = {"/deletefile"})
public class ServletDeleteFile extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<h2>Delete File </h2> ");
        //out.println(request.getParameter("filepath"));
        //out.println(request.getParameter("filename"));

        File fl = new File(request.getParameter("filepath")+request.getParameter("filename"));
        fl.delete();
        out.print("<p>DONE!</p>");
        out.print("<a href=\"opendir?dir=" + request.getParameter("filepath") + "\"> Press for back </a>");

        out.flush();
        out.close();

    }

}
