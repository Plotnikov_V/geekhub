package org.geekhub.storage;

import org.geekhub.objects.Entity;
import org.geekhub.objects.Ignore;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of {@link org.geekhub.storage.Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link org.geekhub.objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        //this method is fully implemented, no need to do anything, it's just an example
        String sql = "SELECT * FROM " + clazz.getSimpleName() + " WHERE id = " + id;
        try(Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        //implement me according to interface by using extractResult method
        String sql = "SELECT * FROM " + clazz.getSimpleName();
        try(Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            statement.close();
            return result;
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        //implement me
        String sql = "DELETE FROM " + entity.getClass().getSimpleName() + " WHERE id = " + entity.getId();
        try(Statement statement = connection.createStatement()) {
            int rowsEffect = statement.executeUpdate(sql);
            statement.close();
            return true;
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);
//        String sql = null;
        if (entity.isNew()) {
            //implement me
            //need to define right SQL query to create object
            String myRows = data.keySet().toString().substring(1, data.keySet().toString().length()-1);
            String myValue = data.values().toString().substring(1, data.values().toString().length()-1);
            String sql = "INSERT INTO "+entity.getClass().getSimpleName()+"(" + myRows + ") VALUES (" + myValue + ")";
            PreparedStatement pstatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstatement.executeUpdate();
            ResultSet generatedKey = pstatement.getGeneratedKeys();
            if (generatedKey.next()) {
                entity.setId(generatedKey.getInt(1));
            }
            generatedKey.close();
            pstatement.close();
        } else {
            //implement me
            //need to define right SQL query to update object
            String myRowsValue = data.entrySet().toString().substring(1, data.entrySet().toString().length()-1);
            String sql = "UPDATE " + entity.getClass().getSimpleName() + " SET " + myRowsValue + " WHERE id=?";
            PreparedStatement pstatement = connection.prepareStatement(sql);
            pstatement.setInt(1, entity.getId());
            pstatement.executeUpdate();
            pstatement.close();
        }
        //implement me, need to save/update object and update it with new id if it's a creation

    }

    //converts object to map, could be helpful in save method
    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        //implement me
        Map<String, Object> tempMap = new HashMap<>();
        Class<?> myClass = entity.getClass();
        Field[] fields = myClass.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            if (!field.isAnnotationPresent(Ignore.class)) {
                if (field.get(entity).getClass().equals(String.class)) {
                    tempMap.put(field.getName(), "\"" + field.get(entity) + "\"");
                } else
                    if (field.get(entity).getClass().equals(boolean.class)) {
                        if (field.get(entity).equals(true)) tempMap.put(field.getName(),1);
                        if (field.get(entity).equals(false)) tempMap.put(field.getName(),0);
                    } else {
                        tempMap.put(field.getName(), field.get(entity));
                    }
                field.setAccessible(false);
            }
        }
        return tempMap;
    }

    //creates list of new instances of clazz by using data from resultset
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultset) throws Exception {
        //implement me
        List list_temp = new ArrayList<>();
        while (resultset.next()) {
            T obj = clazz.newInstance();
            Class<?> myClass = obj.getClass();
            Field[] fields = myClass.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                if (!field.isAnnotationPresent(Ignore.class)) {
                    field.set(obj,resultset.getObject(field.getName()));
                }
                field.setAccessible(false);
            }
            obj.setId(resultset.getInt("id"));
//            org.geekhub.objects.Cat cat = new org.geekhub.objects.Cat();
//            cat.setName(resultset.getString("name"));
//            cat.setAge(resultset.getInt("age"));
            list_temp.add(obj);
        }
        return list_temp;
    }
}
