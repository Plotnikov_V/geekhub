package translator;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import source.*;

import java.io.IOException;
import java.util.Scanner;

@Configuration
@ComponentScan
public class TranslatorController {

    public static void main(String[] args) throws IOException, MyException {

        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");

        SourceLoader sourceLoader = context.getBean(SourceLoader.class);
        Translator translator = context.getBean(Translator.class);

        Scanner scanner = new Scanner(System.in);
        String command = scanner.next();
        while(!"exit".equals(command)) {
            try {
                String source = sourceLoader.loadSource(command);
                String translation = translator.translate(source);
                System.out.println("Original: " + source);
                System.out.println("Translation: " + translation);
            }
            catch (MyException exc) {System.err.println(exc);}
            catch (java.io.FileNotFoundException e) {e.printStackTrace();}
            catch (java.net.MalformedURLException ex) {ex.printStackTrace();}
            command = scanner.next();

        }
    }
}
