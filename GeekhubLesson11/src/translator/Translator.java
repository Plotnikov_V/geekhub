package translator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import source.URLSourceProvider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.net.URLEncoder;

/**
 * Provides utilities for translating texts to russian language.<br/>
 * Uses Yandex Translate API, more information at <a href="http://api.yandex.ru/translate/">http://api.yandex.ru/translate/</a><br/>
 * Depends on {@link URLSourceProvider} for accessing Yandex Translator API service
 */
@Component
public class Translator {
    @Autowired
    private URLSourceProvider urlSourceProvider;
    /**
     * Yandex Translate API key could be obtained at <a href="http://api.yandex.ru/key/form.xml?service=trnsl">http://api.yandex.ru/key/form.xml?service=trnsl</a>
     * to do that you have to be authorized.
     */
    private static final String YANDEX_API_KEY = "trnsl.1.1.20141121T170607Z.8cf9ef3800335151.f7d4da80a0716addfb16a3cdfba78fe8b1dfa13c";
    private static final String TRANSLATION_DIRECTION = "ru";

    @Autowired
    public Translator(URLSourceProvider urlSourceProvider) {

        this.urlSourceProvider = urlSourceProvider;
    }

    /**
     * Translates text to russian language
     * @param original text to translate
     * @return translated text
     * @throws IOException
     */
    public String translate(String original) throws IOException {
        URL url = new URL(prepareURL(original));

        try (BufferedReader breader = new BufferedReader(new InputStreamReader(url.openStream()))//;
        ) {

            String line;
            StringBuilder sbu = new StringBuilder();

            while ((line = breader.readLine()) != null) {
                sbu.append(line);
            }
            breader.close();
            parseContent(sbu.toString());

            return parseContent(sbu.toString());//sbu.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "false";
        }
    }

    /**
     * Prepares URL to invoke Yandex Translate API service for specified text
     * @param text to translate
     * @return url for translation specified text    //encodeText(text)
     */
    private String prepareURL(String text) {
        return "https://translate.yandex.net/api/v1.5/tr/translate?key=" + YANDEX_API_KEY + "&text=" + encodeText(text) + "&lang=" + TRANSLATION_DIRECTION;
    }

    /**
     * Parses content returned by Yandex Translate API service. Removes all tags and system texts. Keeps only translated text.
     * @param content that was received from Yandex Translate API by invoking prepared URL
     * @return translated text
     */
    private String parseContent(String content) {
        //TODO: implement me
        String content_parse = content.substring((content.indexOf("<text>")+6), (content.indexOf("</text>")));
        return content_parse;
    }

    /**
     * Encodes text that need to be translated to put it as URL parameter
     * @param text to be translated
     * @return encoded text
     */
    private String encodeText(String text) {
        //TODO: implement me
        String str_code;
        str_code = URLEncoder.encode(text);
        return str_code;
    }
}
