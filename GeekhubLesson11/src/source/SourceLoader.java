package source;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



/**
 * SourceLoader should contains all implementations of SourceProviders to be able to load different sources.
 */
@Component
public class SourceLoader {

    @Autowired
    private List<SourceProvider> sourceProviders = new ArrayList<>();
    FileSourceProvider file_sp;
    URLSourceProvider url_sp;

    public SourceLoader() {

        this.sourceProviders.add(file_sp);
        this.sourceProviders.add(url_sp);
    }

    public String loadSource(String pathToSource) throws IOException, MyException {

        if (this.sourceProviders.get(0).isAllowed(pathToSource)) {
            return this.sourceProviders.get(0).load(pathToSource);
        }

        if (this.sourceProviders.get(1).isAllowed(pathToSource)) {
            return this.sourceProviders.get(1).load(pathToSource);
        }

        if (!(this.sourceProviders.get(0).isAllowed(pathToSource))&&(!this.sourceProviders.get(1).isAllowed(pathToSource))) {
            throw new MyException("file not found");

        }
        return null;
    }
}
