package source;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths to load are http://someurl.com, https://secureurl.com, ftp://frpurl.com etc.
 */

@Component
public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        try {
            URL url = new URL(pathToSource);
            BufferedReader breader = new BufferedReader(new InputStreamReader(url.openStream()));
        }
        catch (MalformedURLException e) {return false;}
        catch (java.io.FileNotFoundException e) {return false;}
        catch (IOException e) {return false;}
        return true;
    }

    @Override
    public String load(String pathToSource) throws IOException {

        if (isAllowed(pathToSource)) {
            URL url = new URL(pathToSource);
            BufferedReader breader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuilder sbu = new StringBuilder();
            String line;
            while ((line = breader.readLine()) != null) {
                sbu.append(line);
            }
            breader.close();
            return sbu.toString();

        }
        return "not found";
    }
}
