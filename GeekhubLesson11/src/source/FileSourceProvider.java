package source;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */

@Component
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        try {
            FileReader fr = new FileReader(pathToSource);
            fr.close();
        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public String load(String pathToSource) throws IOException {
        //TODO: implement me
        if (isAllowed(pathToSource)) {
            BufferedReader breader = new BufferedReader(new FileReader(pathToSource));
            StringBuilder sbu = new StringBuilder();
            String line;
            while ((line = breader.readLine()) != null) {
                sbu.append(line);
            }
            breader.close();
            return sbu.toString();

        } else {
            return "file not found";
        }
    }
}
