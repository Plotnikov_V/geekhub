package source;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



/**
 * SourceLoader should contains all implementations of SourceProviders to be able to load different sources.
 */
public class SourceLoader {
    private List<SourceProvider> sourceProviders = new ArrayList<>();

    public SourceLoader() {
        FileSourceProvider file_sp = new FileSourceProvider();
        URLSourceProvider url_sp = new URLSourceProvider();
        this.sourceProviders.add(file_sp);
        this.sourceProviders.add(url_sp);
    }

    public String loadSource(String pathToSource) throws IOException, MyException {

        if (this.sourceProviders.get(0).isAllowed(pathToSource)) {
            return this.sourceProviders.get(0).load(pathToSource);
        }

        if (this.sourceProviders.get(1).isAllowed(pathToSource)) {
            return this.sourceProviders.get(1).load(pathToSource);
        }

        if (!(this.sourceProviders.get(0).isAllowed(pathToSource))&&(!this.sourceProviders.get(1).isAllowed(pathToSource))) {
            throw new MyException("file not found");

        }
        return null;
    }
}
