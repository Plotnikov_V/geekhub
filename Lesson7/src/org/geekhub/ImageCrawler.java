package org.geekhub;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * ImageCrawler downloads all images to specified folder from specified resource.
 * It uses multi threading to make process faster. To start download images you should call downloadImages(String urlToPage) method with URL.
 * To shutdown the service you should call stop() method
 */
public class ImageCrawler {

    //number of threads to download images simultaneously
    public static final int NUMBER_OF_THREADS = 10;

    private ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    private String folder;

    public ImageCrawler(String folder) throws MalformedURLException {
        this.folder = folder;
    }

    /**
     * Call this method to start download images from specified URL.
     * @param urlToPage
     * @throws IOException
     */
    public void downloadImages(String urlToPage) throws IOException {
        //implement me
        try {
            URL url = new URL(urlToPage);
            Page page = new Page(url);
            if (page.getImageLinks().size() > 0) {
                for (URL url_im : page.getImageLinks()) {
                    if (isImageURL(url_im)) {
                        Runnable iTask = new ImageTask(url_im, this.folder);
                        executorService.execute(iTask);
                    }
                }
            }
        }
        catch (MalformedURLException e) {System.out.println("No such URL, try again: ");}

    }

    /**
     * Call this method before shutdown an application
     */
    public void stop() {
        executorService.shutdown();
    }

    //detects is current url is an image. Checking for popular extensions should be enough
    private boolean isImageURL(URL url) {
        //implement me
        try {
            URLConnection urlcon = url.openConnection();
            InputStream in = urlcon.getInputStream();
            in.close();

        }
        catch (MalformedURLException e) {return false;}
        catch (java.io.FileNotFoundException e) {return false;}
        catch (IOException e) {return false;}

        return true;
    }



}
