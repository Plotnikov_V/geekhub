package org.geekhub;

import java.io.*;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Main {

    public static final String FOLDER_TO_DOWNLOAD = "d:/images/";

    public static void main(String[] args) throws IOException {
        ImageCrawler imageCrawler = new ImageCrawler(FOLDER_TO_DOWNLOAD);
        try {
            imageCrawler.downloadImages("http://trinixy.ru/16356-prikolnye_kartinki_ochen_mnogo.html");
        } catch(UnknownHostException e) { System.out.println("No such URL, try again: ");}
        System.out.println("While it's loading you can enter another url to start download images:");

        Scanner scanner = new Scanner(System.in);
        String command;
        try {
            while (!"exit".equals(command = scanner.next())) {
                imageCrawler.downloadImages(command);
                System.out.println("...and another url:");
            }
        }catch (NullPointerException e) {System.out.println("No content for download");}
        imageCrawler.stop();
    }
}
