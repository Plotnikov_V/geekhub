package org.geekhub;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Utils class that contains useful method to interact with URLConnection
 */
public class ConnectionUtils {

    /**
     * Downloads content for specified URL and returns it as a byte array.
     * Should be used for small files only. Don't use it to download big files it's dangerous.
     * @param url
     * @return
     * @throws IOException
     */
    public static byte[] getData(URL url) throws IOException {
        //implement me
        try {
            byte[] buf = new byte[1024];
            BufferedInputStream in = new BufferedInputStream(url.openStream());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int i;
            while ((i = in.read(buf)) >= 0) {
                out.write(buf, 0, i);
            }
            return out.toByteArray();
        }
        catch (IllegalArgumentException e) {
            System.out.println("No such URL, try again: ");
            return null;
        }
    }
}
