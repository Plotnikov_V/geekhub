package lesson4;

import java.util.*;

/**
 * Created by root on 13.11.2014.
 */

interface SetOperations {

    public boolean equals(Set a, Set b);

    public Set union(Set a, Set b);

    public Set subtract(Set a, Set b);

    public Set intersect(Set a, Set b);

    public Set symmetricSubtract(Set a, Set b);

}

public class SetDemo implements SetOperations{

    @Override
    public boolean equals(Set a, Set b) {
        boolean flag = false;
        if (a.size() != b.size()) return false;
        else
        {
            for (Object obj_a : a) {
                for (Object obj_b : b) {
                    if (obj_a.equals(obj_b)) {
                        flag = true;
                        break;
                    } else flag = false;
                }
                if (!flag) return false;
            }
            return true;
        }
    }

    @Override
    public Set union(Set a, Set b) {
        Set <Integer> tempSet = new HashSet<Integer>();
        tempSet.addAll(a);
        tempSet.addAll(b);
        return tempSet;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set <Integer> tempSet = new HashSet<Integer>();
        tempSet.addAll(a);
        for (Object obj_a : a) {
            for (Object obj_b : b) {
                if (obj_a.equals(obj_b)) {
                    tempSet.remove(obj_a);
                    break;
                }
            }
        }
        return tempSet;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set <Integer> tempSet = new HashSet<Integer>();
        Set <Integer> tempSet2 = new HashSet<Integer>();
        boolean flag_set_a;
        boolean flag_set_b;
        tempSet.addAll(a);
        tempSet.addAll(b);
        tempSet2.addAll(tempSet);
        for (Object obj_temp:tempSet) {
            flag_set_a = false;
            flag_set_b = false;
            for (Object obj_a : a) {
                if (obj_temp.equals(obj_a)) {
                    flag_set_a = true;
                    break;
                }
            }
            for (Object obj_b : b) {
                if (obj_temp.equals(obj_b)) {
                    flag_set_b=true;
                    break;
                }
            }
            if ((flag_set_a)&&(flag_set_b)) {}
                else tempSet2.remove(obj_temp);
        }
        return tempSet2;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        Set <Integer> tempSet = new HashSet<Integer>();
        Set <Integer> tempABSet = new HashSet<Integer>();
        Set <Integer> tempBASet = new HashSet<Integer>();
        tempSet = union(subtract(a, b), subtract(b, a));
        return tempSet;
    }

    public static Set createMySet(Set<Integer> hs, int indexr) {
        hs.add(2+indexr);
        hs.add(5+indexr);
        hs.add(9+indexr);
        hs.add(19+indexr);
        hs.add(23+indexr);
        return hs;
    }

    public static void main(String[] args) {
        Set <Integer> aSet = new HashSet<Integer>();
        int index_resize=0;
        createMySet(aSet, index_resize);
        aSet.add(1);
        aSet.add(33);
        System.out.println("Set A  "+aSet);

        Set <Integer> bSet = new HashSet<Integer>();
        createMySet(bSet, index_resize+0);
        bSet.add(7);
        bSet.add(15);
        bSet.add(25);
        System.out.println("Set B  "+bSet);

        SetDemo sd = new SetDemo();
        System.out.println("Set A equals Set B =  "+sd.equals(aSet, bSet));

//        Set <Integer> UnionSet = new HashSet<Integer>();
        Set UnionSet = new HashSet<Integer>();
        UnionSet = sd.union(aSet, bSet);
        System.out.println("Set union A&B  "+UnionSet);

        Set SubtractSet = new HashSet<Integer>();
        SubtractSet = sd.subtract(aSet, bSet);
        System.out.println("Set subtract AB  "+SubtractSet);

        Set IntersectSet = new HashSet<Integer>();
        IntersectSet = sd.intersect(aSet, bSet);
        System.out.println("Set intersect AB  "+IntersectSet);

        Set SymSubSet = new HashSet<Integer>();
        SymSubSet = sd.symmetricSubtract(aSet, bSet);
        System.out.println("Set symmetricSubtract AB  "+SymSubSet);
    }
}
