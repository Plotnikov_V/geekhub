package les02;

import java.io.IOException;
import java.util.Objects;

/**
 * Created by root on 24.10.2014.
 */

interface Driveable {
    void accelerate();
    void brake();
    void turn();
}

interface ForceBlok {
    void startEngine();
    void stopEngine();
}

interface FuelEnergyBlok {
    void fill_fuil();
    void refill_bak();
}

interface EnergyAcceptorBlok {
    void spinning();
    void notspinning();
}

class Engine implements ForceBlok{
    double capacity;
    Gas_Tank ob_gt;
    Wheels ob_wh;

    Engine(double c, Gas_Tank gs, Wheels wh){
        capacity = c;
        ob_gt = gs;
        ob_wh = wh;
    }

    @Override
    public void startEngine() {
        System.out.println("engine on");
        ob_gt.fill();
        ob_wh.spinning();
    }

    @Override
    public void stopEngine() {
        System.out.println("engine off");
        ob_wh.notspinning();
    }
}

class Wheels {
    int size_wheel;

    Wheels(int size) {
        size_wheel = size;
    }

    void spinning() {
      //  System.out.println("wheels spinning....");
    }
    void notspinning() {
      //  System.out.println("wheels stopped");
    }
    void turnwhells() {
        System.out.println("You turn on the car");
    }

}

class Gas_Tank {
    int level_gas; //  0 to 100 %

    Gas_Tank(int lev) {
        level_gas = lev;
    }

    void fill() {
      //  System.out.println("refill tank");
        if (level_gas>0) {
            level_gas--;
        }
            else getOil();
    }

    void getOil() {
      //  System.out.println("spent fuel");
        level_gas = 100;
    }

}

abstract class Vehicle implements Driveable {
    abstract void get_distance_traveld();
    abstract void set_speed();

    Vehicle(){
    }

    public void accelerate() {
//        System.out.println("accelerating...car is running");
//        engine.start(gas_tank,wheels);
    }

    public void brake() {
//        System.out.println("braking... car stopped");
//        engine.stop(wheels);
    }

    public void turn() {
//        System.out.println("turning...");
//        wheels.turnwhells();
    }
}

class Car extends Vehicle {
    Engine engine;
    Wheels wheels;
    Gas_Tank gas_tank;

    Car() {
        wheels = new Wheels(15);
        gas_tank = new Gas_Tank(100);
        engine = new Engine(1.2, gas_tank, wheels);

    }
    public void accelerate() {
        System.out.println("accelerating...car is running");
        engine.startEngine();

    }

    public void brake() {
        System.out.println("braking... car stopped");
        engine.stopEngine();
    }

    public void turn() {
        System.out.println("turning...");
        wheels.turnwhells();
    }
    void get_distance_traveld() {
        System.out.println("distance_traveled on Boat is ...");
    }
    void set_speed() {
        System.out.println("Boats speed is ...");
    }
}

class Boat extends Vehicle {
    void get_distance_traveld() {
        System.out.println("distance_traveled on Boat is ...");
    }
    void set_speed() {
        System.out.println("Boats speed is ...");
    }
    public void accelerate() {
        System.out.println("Boats accelerating ...");
    }
    public void brake() {
        System.out.println("Boats braking ...");
    }
    public void turn() {
        System.out.println("Boats made turn ...");
    }
}

class Solar_Powered_Car extends Vehicle {
    int battery_capacity;
    void get_distance_traveld() {
        System.out.println("distance_traveled on Solar Power Car is ...");
    }
    void set_speed() {
        System.out.println("Set speed Solar Power Car");
    }

}

public class MainDemo {
    public static void main(String[] args) throws IOException {
        boolean flag_exit = true;
        char choice;
        System.out.println("You drive car.");
        System.out.println("For exit press 0");
        Car car = new Car();
        System.out.println("For accelerate. Press 1 ");
        System.out.println("For brake. Press 2 ");
        System.out.println("For turn. Press 3 ");
        do {
            choice=(char)System.in.read();
            switch (choice) {
                case '0':
                    flag_exit = false;
                    break;
                case '1':
                    car.accelerate();
                    System.out.println("For exit press 0");
                    System.out.println("For brake. Press 2 ");
                    System.out.println("For turn. Press 3 ");
                    break;
                case '2':
                    car.brake();
                    System.out.println("For exit press 0");
                    System.out.println("For accelerate. Press 1 ");
                    System.out.println("For turn. Press 3 ");
                    break;
                case '3':
                    car.turn();
                    System.out.println("For exit press 0");
                    System.out.println("For accelerate. Press 1 ");
                    System.out.println("For brake. Press 2 ");
                    break;
            }
        } while (flag_exit);

        System.out.println("Creating Boat");
        Boat boat = new Boat();
        boat.set_speed();

        System.out.println("Creating Solar Powered Car");
        Solar_Powered_Car spcar = new Solar_Powered_Car();
        spcar.get_distance_traveld();

    }
}
