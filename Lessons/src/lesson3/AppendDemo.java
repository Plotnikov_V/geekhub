package lesson3;

/**
 * Created by root on 03.11.2014.
 */
public class AppendDemo {
    public static void main(String[] args) {
        long time_begin = 0;
        int limit = 10000;

        System.out.println("Programm Demo time concatenation String, StringBuffer, StringBuilder");
        String str = " ";
        time_begin = System.currentTimeMillis();
        for (int i=1; i<=limit; i++) {
            str+=i;
        }
        System.out.println("String's Time  "+ (System.currentTimeMillis() - time_begin));

        StringBuffer sb = new StringBuffer();
        time_begin = System.currentTimeMillis();
        for (int i=1; i<=limit; i++) {
            sb.append(i);
        }
        System.out.println("StringBuffer's Time  "+ (System.currentTimeMillis() - time_begin));

        StringBuilder sbu = new StringBuilder();
        time_begin = System.currentTimeMillis();
        for (int i=1; i<=limit; i++) {
            sbu.append(i);
        }
        System.out.println("StringBuilder's Time  "+ (System.currentTimeMillis() - time_begin));



    }
}