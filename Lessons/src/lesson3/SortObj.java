package lesson3;


/**
 * Created by root on 03.11.2014.
 */
class Battery implements Comparable {
    int weight, power, voltage;
    Battery (int v, int p, int w) {
        voltage = v;
        power = p;
        weight = w;
    }

    @Override
    public int compareTo(Object o) {
        Battery battery1 = (Battery) o;
        if (battery1.weight != this.weight) {
            if (this.weight < battery1.weight) {
                return -1;
            } else {
                return 1;
            }
        }
        return 0;
    }
}

class SortObj {

    public static void Sort(Battery arrayTemp[]) {
        Battery[] arraySort = new Battery[10];
        for (int i=0 ; i<10; i++) {
            arraySort[i] = new Battery(0,0,0);
            arraySort[i].voltage= arrayTemp[i].voltage;
            arraySort[i].power = arrayTemp[i].power;
            arraySort[i].weight = arrayTemp[i].weight;
        }
        //bubble sorting
        for (int i = 9; i >= 1; i--) {
            boolean sorted = true;
            for (int j = 0; j < i; j++) {
                if (arraySort[j].compareTo(arraySort[j+1]) == 1 ) {
                    int temp_voltage = arraySort[j].voltage;
                    int temp_power = arraySort[j].power;
                    int temp_weight = arraySort[j].weight;
                    arraySort[j].voltage= arraySort[j+1].voltage;
                    arraySort[j].power = arraySort[j+1].power;
                    arraySort[j].weight = arraySort[j+1].weight;
                    arraySort[j+1].voltage = temp_voltage;
                    arraySort[j+1].power = temp_power;
                    arraySort[j+1].weight = temp_weight;
                    sorted = false;
                }
            }
            if(sorted) {
                break;
            }
        }
         //end bubble sorting
        System.out.println("Battery SORT by weight :");
        for (int i=0 ; i<10; i++) {
            System.out.println(arraySort[i].voltage + "V  " + arraySort[i].power + "Ah  " + arraySort[i].weight + "Kg");
        }
    }

    public static void main (String[] args){
        Battery[] arrayNonSort = new Battery[10];
        System.out.println("Battery Non Sort by weight : ");
        for (int i=0 ; i<10; i++) {
            arrayNonSort[i] = new Battery(12, (int) (Math.random() * 70 + 1), (int) (Math.random() * 30 + 1));
            System.out.println(arrayNonSort[i].voltage+"V  "+arrayNonSort[i].power+"Ah  "+arrayNonSort[i].weight+"Kg" );
        }
        Sort(arrayNonSort);
        System.out.println("First array Battery non sort : ");
        for (int i=0 ; i<10; i++) {
            System.out.println(arrayNonSort[i].voltage+"V  "+arrayNonSort[i].power+"Ah  "+arrayNonSort[i].weight+"Kg" );
        }

    }
}
