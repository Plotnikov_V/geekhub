package lesson3;

/**
 * Created by root on 05.11.2014.
 */

import java.io.IOException;

interface Driveable {
    void accelerate() throws ExceptionLevelBak, IOException, ExceptionSpeed;
    void brake() throws ExceptionSpeed;
    void turn();
}

interface ForceBlok {
    void startEngine() throws ExceptionLevelBak, IOException;
    void stopEngine();
}

interface FuelEnergyBlok {
    void fill_fuil() throws ExceptionLevelBak;
    void refill_bak() throws IOException, ExceptionLevelBak;
}

interface EnergyAcceptorBlok {
    void spinning();
    void notspinning();
}

class ExceptionLevelBak extends Exception {
    private int excep;

    ExceptionLevelBak(int excep) {
        this.excep = excep;
    }

    @Override
    public String toString() {
        return "Exception Level Bak, level fuil = "+excep;
    }
}

class ExceptionSpeed extends  Exception {

    @Override
    public String toString() {
        return "Exception speed";
    }
}

class Engine implements ForceBlok{
    double capacity;
    Gas_Tank ob_gt;
    Wheels ob_wh;

    Engine(double c, Gas_Tank gs, Wheels wh){
        capacity = c;
        ob_gt = gs;
        ob_wh = wh;
    }

    @Override
    public void startEngine() throws IOException,ExceptionLevelBak {
        try {
        ob_gt.fill_fuil();
        } catch (ExceptionLevelBak e) {
            System.out.println("Error = "+e);
            ob_gt.refill_bak();
            System.out.println("Уровень топлива  "+ob_gt.level_gas);
            }
        ob_wh.spinning();
    }

    @Override
    public void stopEngine() {
        ob_wh.notspinning();
    }
}

class Wheels implements EnergyAcceptorBlok{
    int size_wheel;

    Wheels(int size) {
        size_wheel = size;
    }

    @Override
    public void spinning() {
//        System.out.println("wheels spinning....");
    }

    @Override
    public void notspinning() {
        System.out.println("wheels stopped");
    }

    void turnwhells() {
        System.out.println("You turn on the car");
    }

}

class Gas_Tank implements FuelEnergyBlok {
    int size_bak;
    int level_gas;

    Gas_Tank(int sb, int lev) {
        size_bak = sb;
        level_gas = lev;
    }

    @Override
    public void fill_fuil() throws ExceptionLevelBak {
        //  System.out.println("refill tank");
        if (level_gas<=0) {
            throw  new ExceptionLevelBak(level_gas);//level_gas--;
        }
        else level_gas--;
    }

    @Override
    public void refill_bak() throws IOException {
       char choice_bak;
       boolean flag_bak=true;
        System.out.println("fill the tank:");
        System.out.println("Press '8' for 10(l) fuil");
        System.out.println("Press '9' for end refill");
        do {
            choice_bak=(char)System.in.read();
            switch (choice_bak) {
                case '9':
                    flag_bak = false;
                    break;
                case '8':
                    level_gas += 10;
                    System.out.println("your fuel level " + level_gas);
                    System.out.println("Press '8' for 10(l) fuil");
                    System.out.println("Press '9' for END refill");
                    break;
            }
        } while ((level_gas < size_bak) && (flag_bak));
    }
}

abstract class Vehicle implements Driveable {
    abstract void get_distance_traveld();
    abstract void set_speed();

    Vehicle(){
    }

    public void accelerate() throws ExceptionLevelBak, IOException, ExceptionSpeed {
//        System.out.println("accelerating... running");
    }

    public void brake() throws ExceptionSpeed {
//        System.out.println("braking... ");
    }

    public void turn() {
//        System.out.println("turning...");
    }
}

class Car extends Vehicle {
    Engine engine;
    Wheels wheels;
    Gas_Tank gas_tank;
    int speedcar = 0;

    Car() {
        wheels = new Wheels(15);
        gas_tank = new Gas_Tank(55, 5);
        engine = new Engine(1.2, gas_tank, wheels);

    }
    public void accelerate() throws ExceptionLevelBak, ExceptionSpeed, IOException {
        System.out.println("accelerating...");
        engine.startEngine();
        speedcar += 10;
        if (speedcar>100) throw new ExceptionSpeed();
        System.out.println("Cars Speed = " + speedcar);
    }

    public void brake() throws ExceptionSpeed {
        System.out.println("braking... ");
        speedcar-=10;
        if (speedcar==0) engine.stopEngine();
        if (speedcar <0) throw new ExceptionSpeed();
        System.out.println("Cars Speed = "+speedcar);
    }

    public void turn() {
        System.out.println("turning...");
        wheels.turnwhells();
    }
    void get_distance_traveld() {
        System.out.println("distance_traveled is ...");
    }
    void set_speed() {
        System.out.println("speed is ...");
    }
}

public class MainDemo {
    static void start_menu(){
        System.out.println("For HELP press '?'");
        System.out.println("For accelerate. Press 1 ");
        System.out.println("For brake. Press 2 ");
        System.out.println("For turn. Press 3 ");

    }
    public static void main(String[] args) throws IOException, ExceptionLevelBak, ExceptionSpeed {
        boolean flag_exit = true;
        char choice;
        Car car = new Car();
        System.out.println("You drive car.");
        start_menu();
        do {
            choice=(char)System.in.read();
            switch (choice) {
                case '0':
                    flag_exit = false;
                    break;
                case '1':
                    try {
                        car.accelerate();
                    } catch (ExceptionSpeed e) {
                        System.out.println("Warning speed limit 100, NO MORE!!!!");
                        car.speedcar-=10;
                        System.out.println("Cars Speed = "+car.speedcar);
                    }
                    start_menu();
                    break;
                case '2':
                    try {
                        car.brake();
                    } catch (ExceptionSpeed e) {
                        System.out.println("Car has stopped !!");
                        car.speedcar = 0;
                        System.out.println("Cars Speed = "+car.speedcar);
                    }
                    start_menu();
                    break;
                case '3':
                    car.turn();
                    start_menu();
                    break;
                case 'f':
                    car.gas_tank.refill_bak();
                    start_menu();
                    break;
                case '?':
                    start_menu();
                    System.out.println("For fill the tank, press 'f'");
                    System.out.println("For exit press 0");
                    break;
            }
        } while (flag_exit);

    }
}
