package les01;

/**
 * Created by root on 24.10.2014.
 */

import java.util.*;
import java.lang.*;
import java.io.*;


public class Fibonachi {
    public static void main(String[] args) throws IOException {
        boolean flagNum = true;
        int n = 0;
        long fib_line[];

        System.out.println("Выводим ряд Фибоначчи для числа");
        do {
            System.out.println("введите число :");
            Scanner sc = new Scanner(System.in);

            if (sc.hasNextInt())  {
                n = sc.nextInt();
                if ((n!=0) && (n<93) && (n>-93)) {
                    flagNum = false;
                } else System.out.println("число должно быть > -93, не 0, <93 ");
            } else {
                System.out.println("Некорректные исходные данные!");
            }
        }
        while (flagNum);

        if ((n==1)||(n==-1)) {
            System.out.println("Ряд Фибоначчи для n= "+n+": ");
            System.out.println("0  1");
        }
        if (n>1) {
            fib_line = new long [n+1];
            fib_line[0] = 0;
            fib_line[1] = 1;
            System.out.println("Ряд Фибоначчи для n= "+n+" : ");
            System.out.print(fib_line[0]+"  "+fib_line[1]);
            for (int i=2; i<n+1; i++ ) {
                fib_line[i] = fib_line[i-1]+fib_line[i-2];
                System.out.print("  "+fib_line[i]);
            }
            System.out.println();
        }

        if (n<-1) {
            fib_line = new long [Math.abs(n)+1];
            fib_line[0] = 0;
            fib_line[1] = 1;
            System.out.println("Ряд Фибоначчи для n= "+n+" : ");
            System.out.print(fib_line[0]+"   "+fib_line[1]);
            for (int i=2; i<Math.abs(n)+1; i++ ) {
                fib_line[i] = fib_line[i-2]-fib_line[i-1];
                System.out.print("  "+fib_line[i]);
            }
            System.out.println();
        }

        System.out.println("нажмите любую кнопку для выхода");
        System.in.read();//ждём нажатия эни кея(throws IOException)
    }
}